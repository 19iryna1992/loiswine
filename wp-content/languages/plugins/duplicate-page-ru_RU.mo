��          �   %   �      `  V   a     �     �     �     �     �     �     �               4  ;   J     �     �     �     �     �     �     �  ?   �     $     -     ?  ,   X  -   �     �  �  �  �   K     $      >     _  )        �     �     �  ;   �  =     ?   \  �   �     %	     C	     a	     	     �	     �	     �	  y   �	     Z
  '   m
  N   �
  ,   �
  0        B                                          	      
                                                                            Add a suffix for duplicate or clone post as Copy, Clone etc. It will show after title. All Editors Buy PRO Choose Editor Classic Editor Donate Draft Duplicate Page Duplicate Page Settings  Duplicate Post Status Duplicate Post Suffix Duplicate Posts, Pages and Custom Posts using single click. Duplicate This Duplicate This as  Duplicate this as  Gutenberg Editor Pending Private Publish Redirect to after click on <strong>Duplicate This Link</strong> Settings To All Posts List To Duplicate Edit Screen https://profiles.wordpress.org/mndpsingh287/ https://wordpress.org/plugins/duplicate-page/ mndpsingh287 PO-Revision-Date: 2021-09-28 14:53:28+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: ru
Project-Id-Version: Plugins - Duplicate Page - Development (trunk)
 Добавьте суффикс для дублирования или клонирования сообщения как Copy, Clone и т. д. Он будет отображаться после заголовка. Все редакторы Купить ПРО версию Выбрать редактор Классический редактор Пожертвовать Черновик Duplicate Page Настройки дублирования страниц  Статус записи после дублирования Суффикс записи после дублирования Дублируйте записи, страницы и пользовательские записи одним щелчком мыши. Дублировать это Дублировать как Дублировать как Редактор Gutenberg Ожидает проверки Личный Опубликовать Перенаправление после нажатия на <strong>Дублировать эту ссылку</strong> Настройки К списку всех записей Перейти к экрану редактирования дубликата https://profiles.wordpress.org/mndpsingh287/ https://ru.wordpress.org/plugins/duplicate-page/ mndpsingh287 