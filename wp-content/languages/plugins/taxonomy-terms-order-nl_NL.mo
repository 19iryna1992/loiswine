��    &      L  5   |      P  &   Q  
   x     �     �  	   �     �  '   �  	   �     �  �   �  D   �     �     �     �     �           $     -     1  �   4  	   �     �     �  
   �  
   �     �            S   ,  >   �     �     �     �     �     �  E     W   X  @  �  (   �     	  	   *	     4	     ;	  !   P	  '   r	     �	  	   �	  �   �	  F   B
     �
  	   �
     �
     �
  :   �
     �
            �   	     �     �     �     �     �     �          *  j   :  G   �     �                    7  Y   P  k   �                                                                             !      %      "                 	   
                             &      #      $                            Additional description and details at  Admin Sort Administrator Author Auto Sort Auto Sort Description Category Order and Taxonomy Terms Order Check our Contributor Did you find this plugin useful? Please support our work with a donation or write an article about this plugin in your blog with a link to our site Did you know there is available an advanced version of this plug-in? Editor General General Settings Items Order Updated Minimum Level to use this plugin Nsp-Code OFF ON Order Categories and all custom taxonomies terms (hierarchically) and child terms using a Drag and Drop Sortable javascript capability. Read more Save Settings Settings Saved Subscriber Taxonomies Taxonomy Order Taxonomy Terms Order Taxonomy Title This plugin can't work without javascript, because it's use drag and drop and AJAX. This will change the order of terms within the admin interface Total Posts Update global setting http://www.nsp-code.com https://www.nsp-code.com plugin which allow to custom sort all posts, pages, custom post types plugin which allow to custom sort categories and custom taxonomies terms per post basis PO-Revision-Date: 2021-10-02 14:14:45+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - Category Order and Taxonomy Terms Order - Stable (latest release)
 Aanvullende beschrijving en details bij  Beheer sorteren Beheerder Auteur Automatisch sorteren Beschrijving automatisch sorteren Category Order and Taxonomy Terms Order Bekijk onze Bijdrager Vind je deze module handig? Ondersteun ons werk met een donatie of schrijf een artikel over deze plugin in je blog met een link naar onze website Wist je dat er een geavanceerde versie van deze plugin beschikbaar is? Bewerker Standaard Instellingen Volgorde bijgewerkt Minimum vereiste gebruikersgroep om de plugin te gebruiken Nsp-Code UIT AAN Sorteer categorieën en alle aangepaste termen voor taxonomieën (hiërarchisch) en sub termen met behulp van een drag & drop sorteerbare JavaScript mogelijkheid. Lees verder Instellingen opslaan Instellingen Opgeslagen Abonnee Taxonomiën Taxonomie volgorde Volgorde van taxonomie termen Taxonomie titel Deze plugin kan niet werken zonder javascript, want het is gebruik maken van slepen en neerzetten en AJAX. Dit zal de volgorde van termen binnen de beheerder interface veranderen Totaal aantal berichten Update globale instelling http://www.nsp-code.com https://www.nsp-code.com plugin waarmee je alle berichten, pagina's, aangepaste berichttypen op maat kunt sorteren plugin die het mogelijk maakt om aangepaste categorieën en aangepaste taxonomieën per bericht te sorteren 