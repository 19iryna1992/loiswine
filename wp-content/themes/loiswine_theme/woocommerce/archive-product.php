<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;


get_header('shop'); ?>

<?php do_action('woocommerce_before_main_content'); ?>
    <div class="container">
        <div class="row pt-3">
            <div class="col-9 col-md-10">
                <div class="c-breadcrumb">
                    <?php woocommerce_breadcrumb(); ?>
                </div>
            </div>
            <div class="col-12">
                <div class="c-category-description">
                    <?php do_action('woocommerce_archive_description'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="JS-shop-sidebar-mob c-shop-sidebar-mob d-lg-none">
        <div class="c-shop-sidebar-mob__btn-wrap">
            <button class="c-shop-sidebar-mob__btn"><img
                        src="/wp-content/themes/loiswine_theme/img/icons/black-filter.svg"
                        alt="loiswine_filter"></button>
        </div>
        <?php if (is_active_sidebar('shop-sidebar')) : ?>
            <?php get_template_part('template-parts/organisms/shop-sidebar'); ?>
        <?php endif; ?>
    </div>
    <div class="container">
        <div class="row justify-content-lg-between">
            <?php if (is_active_sidebar('shop-sidebar')) : ?>
                <div class="d-none d-lg-block col-lg-3">
                    <?php get_template_part('template-parts/organisms/shop-sidebar'); ?>
                </div>
            <?php endif; ?>
            <div class="col-12 col-lg-9">

                <?php if (woocommerce_product_loop()) {

                    do_action('woocommerce_before_shop_loop');

                    woocommerce_product_loop_start();

                    if (wc_get_loop_prop('total')) {
                        while (have_posts()) {
                            the_post();

                            do_action('woocommerce_shop_loop'); ?>

                            <div class="col-6 col-md-3">
                                <?php echo wc_get_template_part('content', 'product'); ?>
                            </div>
                        <?php }
                    }

                    woocommerce_product_loop_end();

                    do_action('woocommerce_after_shop_loop');
                } else {

                    do_action('woocommerce_no_products_found');
                }

                do_action('woocommerce_after_main_content');
                ?>
            </div>
        </div>
    </div>
<?php get_footer('shop');
