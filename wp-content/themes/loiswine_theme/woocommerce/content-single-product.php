<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}

$terms = get_the_terms($product->ID, 'brand');

?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class('', $product); ?>>
    <div class="container">
        <div class="row">
            <div class="col-9 col-md-10">
                <div class="c-breadcrumb">
                    <?php woocommerce_breadcrumb(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <?php
                /**
                 * Hook: woocommerce_before_single_product_summary.
                 *
                 * @hooked woocommerce_show_product_sale_flash - 10
                 * @hooked woocommerce_show_product_images - 20
                 */
                do_action('woocommerce_before_single_product_summary');
                ?>
            </div>
            <div class="col-lg-5">
                <?php
                /**
                 * Hook: woocommerce_single_product_summary.
                 *
                 * @hooked woocommerce_template_single_title - 5
                 * @hooked woocommerce_template_single_rating - 10
                 * @hooked woocommerce_template_single_price - 10
                 * @hooked woocommerce_template_single_excerpt - 20
                 * @hooked woocommerce_template_single_add_to_cart - 30
                 * @hooked woocommerce_template_single_meta - 40
                 * @hooked woocommerce_template_single_sharing - 50
                 * @hooked WC_Structured_Data::generate_product_data() - 60
                 */
                do_action('woocommerce_single_product_summary');
                get_template_part('template-parts/components/woocommerce/wine-detail');
                ?>
            </div>
            <div class="col-lg-4">
                <div class="s-single-product-price__card">
                    <div class="s-single-product-price__wrap">
                        <?php woocommerce_template_single_price() ?>
                    </div>
                    <div class="s-single-product-price__add-to-card">
                        <?php woocommerce_template_single_add_to_cart(); ?>
                    </div>
                    <?php get_template_part('template-parts/components/woocommerce/delivery-info'); ?>

                    <?php get_template_part('template-parts/components/woocommerce/payment-information'); ?>
                </div>
            </div>
        </div>
        <div class="s-single-product__description">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8">
                    <div class="c-wysiwyg">
                        <?php echo get_the_content() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($terms)):
    $brand_img = get_field('brand_image_for_product_page', $terms[0]);
    $brand_description = get_field('brand_description_for_product_page', $terms[0]);
    ?>
    <div class="s-single-product__winemaker u-bg-img d-none d-lg-block"
         style="background-image: url('<?= $brand_img ? $brand_img["url"] : "" ?>')">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                    <div class="product-winemaker__card">
                        <?php if ($brand_description): ?>
                            <div class="c-wysiwyg">
                                <?php echo $brand_description ?>
                            </div>
                            <div class="c-btn">
                                <a class="button c-secondary-button"
                                   href="<?php echo get_permalink($terms[0]->term_id); ?>"><?php _e('Read more >>', 'loiswine') ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-lg-none">
        <div class="s-single-product__winemaker u-bg-img"
             style="background-image: url('<?= $brand_img ? $brand_img["url"] : "" ?>'); height: 400px">
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="product-winemaker__card">
                        <?php if ($brand_description): ?>
                            <div class="c-wysiwyg">
                                <?php echo $brand_description ?>
                            </div>
                            <div class="c-btn">
                                <a class="button c-secondary-button"
                                   href="<?php echo get_permalink($terms[0]->term_id); ?>"><?php _e('Read more >>', 'loiswine') ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<div class="container">

    <?php
    /**
     * Hook: woocommerce_after_single_product_summary.
     *
     * @hooked woocommerce_output_product_data_tabs - 10
     * @hooked woocommerce_upsell_display - 15
     * @hooked woocommerce_output_related_products - 20
     */
    do_action('woocommerce_after_single_product_summary');
    ?>
</div>

<?php do_action('woocommerce_after_single_product'); ?>
