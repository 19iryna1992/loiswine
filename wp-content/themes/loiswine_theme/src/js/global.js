(function ($) {

    $(document).ready(function () {

        function setEqualHeight(columns) {
            var tallestcolumn = 0;
            columns.each(
                function () {
                    currentHeight = $(this).height();
                    if (currentHeight > tallestcolumn) {
                        tallestcolumn = currentHeight;
                    }
                }
            );
            columns.height(tallestcolumn);
        }

        setEqualHeight($(".JS--product-card"));

        $('#toggle_menu').click(function () {
            $('#menu_open').addClass('JS--open-mobile-menu');
        });

        $('#toggle_menu_close').click(function () {
            $('#menu_open').removeClass('JS--open-mobile-menu');
        });

        // Sidebar shop

        var filtersBtn = $(".c-shop-sidebar-mob__btn ");

        for (var i = 0; i < filtersBtn.length; i++) {
            filtersBtn[i].onclick = function () {
                $(this).toggleClass('is-open');

                var content = $('.JS-shop-sidebar-mob');
                content.toggleClass('is-open-sidebar');
            }
        }

        // Sidebar winemakers

        var filtersBtn = $(".c-brand-sidebar-mob__btn ");

        for (var i = 0; i < filtersBtn.length; i++) {
            filtersBtn[i].onclick = function () {
                $(this).toggleClass('is-open');

                var content = $('.JS-brand-sidebar-mob');
                content.toggleClass('is-open-sidebar');
            }
        }

        // Accordion

        var accordions = document.getElementsByClassName("c-accordion__btn");

        for (var i = 0; i < accordions.length; i++) {
            accordions[i].onclick = function () {
                this.classList.toggle('is-open');

                var content = this.nextElementSibling;
                if (content.style.maxHeight) {
                    content.style.maxHeight = null;
                } else {
                    content.style.maxHeight = content.scrollHeight + "px";
                }
            }
        }

        /*------------------------
            Selectric
         ------------------------*/
        $('select').selectric('init');

        $('select').each(function () {
                $(this).selectric();
            }
        );

        $('select').selectric({
            disableOnMobile: false,
            nativeOnMobile: false
        });

        /*------------------------
         Input Number Style
         ------------------------*/

        $('input[type="number"]').bootstrapNumber();

        $('input[type="number"]').bootstrapNumber({
            // default, danger, success , warning, info, primary
            upClass: 'danger',
            downClass: 'success',
            center: true
        });


        $('.JS-event').click(function () {
            $('.JS-modal').toggleClass('d-none');
        });
        $('.JS-modal').click(function () {
            $(this).toggleClass('d-none');
        });


    });


})(jQuery);



