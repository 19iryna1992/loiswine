(function ($) {

    $(document).ready(function () {

        $('.c-product-slider').each(function () {

            var productSliderPagination = $(this).find('.swiper-pagination')
            var simpleSlider = new Swiper($(this).find('.swiper-container'), {
                slidesPerView: 2,
                spaceBetween: 5,
                autoPlay: false,
                breakpoints: {
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    768: {
                        slidesPerView: 5,
                        spaceBetween: 40,
                    }
                },

                pagination: {
                    el: productSliderPagination,
                    clickable: true,
                },
            });

        });

        $('.JS--main-slider').each(function () {
            console.log(this);

            var mainSliderPagination = $(this).find('.swiper-pagination')
            var mainSlider = new Swiper($(this).find('.swiper-container'), {
                autoPlay: false,

                pagination: {
                    el: mainSliderPagination,
                    clickable: true,
                },
            });

        });


    });

})(jQuery);


