<?php

get_header();

$current_brand = get_queried_object();

$loop = new WP_Query(array(
    'post_type' => 'product',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'tax_query' => array(
        array(
            'taxonomy' => 'brand',
            'field' => 'term_id',
            'terms' => $current_brand->term_id,
        )
    ),
));

// ACF vars

$image = get_field('brand_image', $current_brand);
?>
    <main id="main" role="main" tabindex="-1">
        <div class="JS-brand-sidebar-mob c-brand-sidebar-mob d-lg-none">
            <div class="c-brand-sidebar-mob__btn-wrap">
                <button class="c-brand-sidebar-mob__btn"><img
                            src="/wp-content/themes/loiswine_theme/img/icons/black-filter.svg"
                            alt="loiswine_filter">
                </button>
            </div>
            <?php get_template_part('template-parts/components/brands-list') ?>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="c-breadcrumb">
                        <?php woocommerce_breadcrumb(); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="d-none d-lg-block col-3">
                    <?php get_template_part('template-parts/components/brands-list') ?>
                </div>
                <div class="col-12 col-lg-9">
                    <div class="c-brand__content">
                        <div class="row">
                            <div class="col-12">
                                <div class="c-brand__title">
                                    <h2 class="s-products-cat__title"><?php echo single_term_title() ?></h2>
                                </div>
                            </div>
                            <?php if ($image): ?>
                                <div class="col-12">
                                    <div class="c-brand__img">
                                        <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="col-12">
                                <div class="c-brand__description">
                                    <?php echo term_description($current_brand->term_id); ?>
                                </div>
                            </div>
                        </div>

                        <?php if ($loop->have_posts()) : ?>
                            <div class="row">
                                <?php while ($loop->have_posts()): $loop->the_post(); ?>
                                    <div class="col-6 col-md-3">
                                        <?php wc_get_template_part('content', 'product'); ?>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php get_footer(); ?>