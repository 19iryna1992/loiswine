<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package loiswine
 */

get_header();
?>

<main id="main" role="main" tabindex="-1">

    <?php get_template_part('template-parts/components/breadcrumbs'); ?>

    <?php if (have_posts()): ?>
        <section class="s-blog">
            <div class="container">
                <div class="row">
                    <?php while (have_posts()): the_post();
                        // ACF
                        $description = get_field('blog_small_description');
                        ?>
                        <div class="col-12 col-md-4">
                            <a href="<?php echo get_permalink()?>">
                                <div class="c-post">
                                    <div class="c-post__img u-bg-img"
                                         style="background-image:url('<?php echo get_the_post_thumbnail_url() ?>') ">
                                    </div>
                                    <div class="c-post__wrap">
                                        <h3 class="c-post__title">
                                            <?php echo get_the_title(); ?>
                                        </h3>
                                        <span class="c-post__data">
                                            <?php echo get_the_date(); ?>
                                        </span>
                                    </div>
                                    <?php if ($description): ?>
                                        <div class="c-post__description">
                                            <?php echo $description ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </a>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
</main>


<?php get_footer(); ?>

