<?php

/**
 * loiswine functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package loiswine
 */

require_once __DIR__ . '/inc/functions.php';
require_once __DIR__ . '/post-type/post-types.php';

/*------------------------------------*\
	Theme ACF Page Settings
\*------------------------------------*/

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Header Settings',
        'menu_title' => 'Header',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Theme Footer Settings',
        'menu_title' => 'Footer',
        'parent_slug' => 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title' => 'Theme Woocommerce Settings',
        'menu_title' => 'Woocommerce',
        'parent_slug' => 'theme-general-settings',
    ));

}


/*------------------------------------*\
	Theme Custom Settings
\*------------------------------------*/


if (!function_exists('loiswine_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function loiswine_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Chiquita, use a find and replace
         * to change 'chiquita' to the name of your theme in all the template files.
         */
        load_theme_textdomain('loiswine', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');


        // image sizes
        add_image_size('size_355_520', 355, 520, true);
        add_image_size('size_190_70', 190, 70, true);
        add_image_size('size_400_300', 400, 250, true);
        add_image_size('size_150_150', 150, 150, true);
        add_image_size('size_50_35', 50, 35, true);

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-primary' => esc_html__('Primary', 'loiswine'),
            'main-mobile-menu' => esc_html__('Mobile main menu', 'loiswine'),
            'footer-menu-info' => esc_html__('Footer menu information', 'loiswine'),
            'footer-menu-shop' => esc_html__('Footer menu shop', 'loiswine'),
            'footer-menu-account' => esc_html__('Footer menu account', 'loiswine'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('loiswine_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'loiswine_setup');


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function loiswine_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Shop sidebar', 'loiswine'),
        'id' => 'shop-sidebar',
        'description' => esc_html__('Add widgets here.', 'loiswine'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Brands sidebar', 'loiswine'),
        'id' => 'brands-sidebar',
        'description' => esc_html__('Add widgets here.', 'loiswine'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'loiswine_widgets_init');


/*------------------------------------*\
	Enqueue scripts and styles.
\*------------------------------------*/

function loiswine_scripts()
{

    $script_version = '20151216';
    wp_enqueue_style('loiswine-default-style', get_stylesheet_uri());
    wp_enqueue_style('loiswine-main-style', get_template_directory_uri() . '/dist/css/styles.min.css');
    wp_enqueue_script('loiswine-scripts', get_template_directory_uri() . '/dist/js/scripts.min.js', array('jquery'), $script_version, true);

    if (is_front_page()) {
        wp_enqueue_script('loiswine-home-script', get_template_directory_uri() . '/dist/js/home.min.js', array('jquery'), $script_version, true);
    }
}

add_action('wp_enqueue_scripts', 'loiswine_scripts');


/* Excluding Folders while migrating with All-in-One WP Migration */
add_filter('ai1wm_exclude_content_from_export', function ($exclude_filters) {
    $exclude_filters[] = 'themes/loiswine/node_modules';
    /* $exclude_filters[] = 'updraft'; */
    return $exclude_filters;
});


function load_dashicons()
{
    wp_enqueue_style('dashicons');
}

add_action('wp_enqueue_scripts', 'load_dashicons');



/*------------------------------------*\
	Example comment heading
\*------------------------------------*/








