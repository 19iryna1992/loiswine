<?php if (have_rows('contact')): ?>
    <section class="s-contact">
        <div class="container">
            <?php while (have_rows('contact')) :
                the_row();
                $title = get_sub_field('title_contact');
                $icon = get_sub_field('icon');
                $map = get_sub_field('map');
                $address = get_sub_field('address'); ?>
                <div class="s-contact__row">
                    <div class="row">
                        <?php if ($title): ?>
                            <div class="col-12 col-md-6">
                                <div class="s-contact__title-wrap">
                                    <?php if ($icon): ?>
                                        <img src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt'] ?>">
                                    <?php endif; ?>
                                    <h2 class="s-contact__title"><?php echo $title ?></h2>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($address): ?>
                            <div class="col-12 col-md-6">
                                <div class="s-contact__address">
                                    <?php echo $address ?>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                    <div class="row">
                        <?php if ($map): ?>
                            <div class="col-md-12">
                                <div class="s-contact__map">
                                    <?php echo $map ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </section>
<?php endif; ?>





