<?php
$text = get_field('home_page_wine_story');
$img = get_field('home_page_wine_story_img');
?>

<section class="s-history">
    <div class="container">
        <div class="row">
            <?php if ($img): ?>
                <div class="col-12 col-lg-6 d-flex flex-column justify-content-center">
                    <div class="s-history__img">
                        <img src="<?php echo $img['url'] ?>" alt="<?php echo $img['alt'] ?>">
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($text): ?>
                <div class="col-12 col-lg-6 d-flex flex-column justify-content-center">
                    <div class="s-history__text">
                        <?php echo $text ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
