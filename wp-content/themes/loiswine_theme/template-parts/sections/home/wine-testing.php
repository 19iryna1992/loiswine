<?php
$text = get_field('home_page_testing_wine');
$img = get_field('home_page_wine_testing_img');
$link = get_field('home_page_wine_testing_link');
?>

<section class="s-history u-bg-white">
    <div class="container">
        <div class="row">
            <?php if ($text): ?>
                <div class="col-12 col-lg-6 d-flex flex-column justify-content-center">
                    <div class="s-history__text">
                        <?php echo $text ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($img): ?>
                <div class="col-12 col-lg-6 d-flex flex-column justify-content-center">
                    <div class="s-history__img">
                        <img src="<?php echo $img['url'] ?>" alt="<?php echo $img['alt'] ?>">
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
