<?php

$tax_query[] = array(
    'taxonomy' => 'product_visibility',
    'field' => 'name',
    'terms' => 'featured',
    'operator' => 'IN',
);

$query = new WP_Query(array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'ignore_sticky_posts' => 1,
    'posts_per_page' => -1,
    'tax_query' => $tax_query
));
?>


<?php if ($query->have_posts()) : ?>
    <section class="s-feature-products">
        <div class="container">
            <div class="row justify-content-lg-around">
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="col-lg-2">
                        <div class="c-feature-product">
                            <?php wc_get_template_part( 'content', 'product' );?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php wp_reset_query(); ?>

