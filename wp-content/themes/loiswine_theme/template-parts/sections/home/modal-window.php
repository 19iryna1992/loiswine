<?php
$shortcode = get_field('modal_form_shortcode', 'option');
?>

<section class="s-modal JS-modal d-none">
    <div class="c-modal">
        <?php echo do_shortcode($shortcode) ?>
    </div>
</section>
