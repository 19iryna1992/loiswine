<?php
$section_title = get_field('category_products_section_title');
$loop = new WP_Query(array(
    'post_type' => 'product',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'add_to_product_slider',
            'value' => '1',
            'compare' => '=='
        ),
    )
)); ?>
<?php if ($loop->have_posts()) : ?>
    <section class="s-products-cat">
        <div class="container">
            <?php if ($section_title): ?>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <h2 class="s-products-cat__title">
                            <?php echo $section_title ?>
                        </h2>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row justify-content-center d-none d-lg-flex">
                <?php while ($loop->have_posts()): $loop->the_post(); ?>
                    <div class="col-3">
                        <?php wc_get_template_part('content', 'product'); ?>
                    </div>
                <?php endwhile; ?>

            </div>


            <div class="row justify-content-center d-lg-none">
                <div class="col-12">
                    <div class="c-product-slider swiper-slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php while ($loop->have_posts()): $loop->the_post(); ?>
                                    <div class="swiper-slide">
                                        <?php wc_get_template_part('content', 'product'); ?>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php wp_reset_query(); ?>