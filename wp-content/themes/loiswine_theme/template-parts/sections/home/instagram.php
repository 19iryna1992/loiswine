<?php
$instagram_shortcode = get_field('instagram_shortcode');
?>
<?php if ($instagram_shortcode): ?>
    <section class="s-instagram">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php echo do_shortcode($instagram_shortcode) ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

