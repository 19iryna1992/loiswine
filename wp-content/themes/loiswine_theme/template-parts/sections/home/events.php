<?php
$section_title = get_field('event_section_title');
?>

<section class="s-event">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php if ($section_title): ?>
                    <h2><?php echo $section_title ?> </h2>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <?php if (have_rows('events')): ?>
                <?php while (have_rows('events')) : the_row();
                    $img = get_sub_field('image');
                    $title = get_sub_field('title');
                    $location = get_sub_field('location');
                    $date = get_sub_field('date');
                    ?>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="c-event JS-event">
                            <?php if ($img): ?>
                                <div class="c-event__img">
                                    <img src="<?php echo $img['url'] ?>" alt="<?php echo $img['alt'] ?>">
                                </div>
                            <?php endif; ?>
                            <div class="c-even__wrap">
                                <?php if ($date): ?>
                                    <p class="c-event__date">
                                        <span class="c-event__icon">
                                            <svg width="12" height="12" viewBox="0 0 12 12" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M6 12C2.6915 12 0 9.3085 0 6C0 2.6915 2.6915 0 6 0C9.3085 0 12 2.6915 12 6C12 9.3085 9.3085 12 6 12ZM6 0.5C2.9675 0.5 0.5 2.9675 0.5 6C0.5 9.0325 2.9675 11.5 6 11.5C9.0325 11.5 11.5 9.0325 11.5 6C11.5 2.9675 9.0325 0.5 6 0.5Z"
                                                      fill="black"/>
                                                <path d="M8.75 9C8.686 9 8.622 8.9755 8.573 8.927L5.823 6.177C5.7765 6.13 5.75 6.0665 5.75 6V2.25C5.75 2.112 5.862 2 6 2C6.138 2 6.25 2.112 6.25 2.25V5.8965L8.927 8.5735C9.0245 8.671 9.0245 8.8295 8.927 8.927C8.878 8.9755 8.814 9 8.75 9Z"
                                                      fill="black"/>
                                            </svg>
                                        </span>
                                        <?php echo $date; ?>
                                    </p>
                                <?php endif; ?>
                                <?php if ($title): ?>
                                    <h4 class="c-event__title"><?php echo $title; ?></h4>
                                <?php endif; ?>
                            </div>
                            <?php if ($location): ?>
                                <p class="c-event__location">
                                        <span class="c-event__icon">
                                            <svg width="12" height="12" viewBox="0 0 12 12" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                    <g clip-path="url(#clip0)">
                                                        <path d="M5.99999 12C5.94099 12 5.88199 11.979 5.83449 11.9375C5.65749 11.781 1.49999 8.072 1.49999 4.5C1.49999 2.0185 3.51899 0 5.99999 0C8.48099 0 10.5 2.0185 10.5 4.5C10.5 8.072 6.34249 11.781 6.16549 11.9375C6.11799 11.979 6.05899 12 5.99999 12ZM5.99999 0.5C3.79449 0.5 1.99999 2.2945 1.99999 4.5C1.99999 7.459 5.22549 10.679 5.99999 11.4095C6.77449 10.679 9.99999 7.459 9.99999 4.5C9.99999 2.2945 8.20549 0.5 5.99999 0.5Z"
                                                              fill="black"/>
                                                        <path d="M6.00002 6.99999C4.62152 6.99999 3.50002 5.87849 3.50002 4.49999C3.50002 3.1215 4.62152 2 6.00002 2C7.37852 2 8.50002 3.1215 8.50002 4.49999C8.50002 5.87849 7.37852 6.99999 6.00002 6.99999ZM6.00002 2.5C4.89702 2.5 4.00002 3.397 4.00002 4.49999C4.00002 5.60299 4.89702 6.49999 6.00002 6.49999C7.10302 6.49999 8.00002 5.60299 8.00002 4.49999C8.00002 3.397 7.10302 2.5 6.00002 2.5Z"
                                                              fill="black"/>
                                                    </g>
                                                <defs>
                                                <clipPath id="clip0">
                                                <rect width="12" height="12" fill="white"/>
                                                </clipPath>
                                                </defs>
                                            </svg>
                                        </span>
                                    <?php echo $location; ?>
                                </p>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>

            <?php endif; ?>
        </div>
    </div>
</section>
