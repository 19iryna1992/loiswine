<?php
$section_title = get_field('category_products_section_link_two');
$loop = new WP_Query(array(
    'post_type' => 'product',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'add_to_product_slider_two',
            'value' => '1',
            'compare' => '=='
        ),
    )
)); ?>
<?php if ($loop->have_posts()) : ?>
    <section class="s-products-cat">
        <div class="container">
            <?php if ($section_title): ?>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <a href="<?php echo $section_title['url'] ?>">
                            <h2 class="s-products-cat__title">
                                <?php echo $section_title['title'] ?>
                                <img src="/wp-content/themes/loiswine_theme/img/icons/arrow_primary_color.svg"
                                     alt=" <?php echo $section_title['title'] ?>">
                            </h2>
                        </a>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="c-product-slider swiper-slider">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php while ($loop->have_posts()): $loop->the_post(); ?>
                                    <div class="c-product-card__slide swiper-slide c-product-card">
                                        <div class="c-product-card__wrap">
                                            <?php echo get_the_post_thumbnail($post, $size = 'size_150_150'); ?>
                                            <h4 class=" woocommerce-loop-product__title">
                                                <?php the_title(); ?>
                                            </h4>
                                            <div class="woo-product-short-desc">
                                                <?php echo get_the_excerpt() ?>
                                            </div>
                                            <p class="price">
                                                <?php woocommerce_template_loop_price(); ?>
                                            </p>
                                            <?php woocommerce_template_loop_add_to_cart(); ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php wp_reset_query(); ?>