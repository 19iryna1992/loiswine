<?php if (have_rows('repeater_info_card')): ?>
    <?php
    $i = 0;
    ?>
    <section class="s-info-box">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <?php while (have_rows('repeater_info_card')) : the_row();
                    $background_color = get_sub_field('background_color');
                    $icon = get_sub_field('icon');
                    $title = get_sub_field('title');
                    $title_color = get_sub_field('title_color');
                    $description = get_sub_field('description');
                    $description_color = get_sub_field('description_color');
                    $i++;
                    ?>

                    <div class="col-12 col-md-6 col-lg-3 d-flex m-0 p-0 align-items-center justify-content-center">
                        <div class="s-info-box__item" style="background-color: <?php echo $background_color ?> ">
                            <div class=" <?= $i !== 4 ? 's-info-box__decoration' :  ''?>">
                                <?php if ($icon): ?>
                                    <img class="s-info-box__img" src="<?php echo $icon['url'] ?>"
                                         alt="<?php echo $icon['alt'] ?>">
                                <?php endif; ?>
                                <?php if ($title): ?>
                                    <h2 class="s-info-box__title"
                                        style="color: <?php echo $title_color ?> "><?php echo $title ?></h2>
                                <?php endif; ?>
                                <?php if ($description): ?>
                                    <div class="<!--s-info-box__description-->"
                                         style="color: <?php echo $description_color ?> ">
                                        <?php echo $description ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>

<?php endif; ?>