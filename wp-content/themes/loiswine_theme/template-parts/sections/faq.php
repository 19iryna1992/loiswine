<?php
$bottom_image = get_field('faq_bottom_image');
?>
<section class="s-faq">
    <div class="container">
        <?php if (have_rows('faq_repiater')): ?>
            <div class="row justify-content-center">
                <div class="col-12 col-md-11 col-lg-10 col-xl-10">
                    <div class="c-accordion">
                        <?php while (have_rows('faq_repiater')): the_row();
                            $title = get_sub_field('faq_title');
                            $text = get_sub_field('faq_text');
                            ?>
                            <?php if ($title): ?>
                                <button class="c-accordion__btn">
                                    <?php echo $title ?>
                                </button>
                            <?php endif; ?>
                            <?php if ($text): ?>
                                <div class="c-accordion__content">
                                    <?php echo $text ?>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if($bottom_image): ?>
            <div class="s-faq__blur-img" style="background-image: url('<?php echo $bottom_image ?>')">
                <div class="s-faq__overlay"></div>
            </div>
        <?php endif; ?>
    </div>
</section>