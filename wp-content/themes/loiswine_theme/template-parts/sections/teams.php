<?php
$title = get_field('teams_section_title');
?>
<?php if (have_rows('teams')): ?>
    <section class="s-team">
        <div class="container">
            <div class="row justify-content-center">
                <?php if ($title): ?>
                    <div class="col-12">
                        <h2 class="s-team__title"><?php echo $title ?></h2>
                    </div>
                <?php endif; ?>

                <?php while (have_rows('teams')) : the_row(); ?>
                    <div class="col-lg-3 col-md-6 col-10">
                        <?php get_template_part('template-parts/components/team'); ?>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>