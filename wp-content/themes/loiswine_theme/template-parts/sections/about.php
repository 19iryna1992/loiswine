<?php
$img = get_field('about_image');
$title = get_field('about_title');
$description = get_field('about_text');
?>

<section class="s-about">
    <div class="container">
        <div class="row">
            <?php if ($title): ?>
                <div class="col-12">
                    <h1 class="s-about__title"><?php echo $title ?></h1>
                </div>
            <?php endif; ?>

            <?php if ($img): ?>
                <div class="col-12 col-md-6 col-lg-5">
                    <img class="s-about__img" src="<?php echo $img['url'] ?>" alt="<?php echo $img['alt'] ?>">
                </div>
            <?php endif; ?>
            <?php if ($description): ?>
                <div class="col-12 col-md-6 col-lg-7">
                    <div class="s-about__description">
                        <?php echo $description ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>