<div class="container">
    <div class="row pt-3">
        <div class="col-9 col-md-10">
            <?php if (function_exists('yoast_breadcrumb')) :
                yoast_breadcrumb('<div class="c-breadcrumb" id="breadcrumbs">', '</div>');
            endif ?>
        </div>
    </div>
</div>
