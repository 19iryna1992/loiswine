<?php
$team_image = get_sub_field('team_image');
$team_name = get_sub_field('team_name');
$team_position = get_sub_field('team_position');
?>

<div class="c-team">
    <?php if ($team_image): ?>
        <div class="c-team__img">
            <img src="<?php echo $team_image['url'] ?>" alt="<?php echo $team_image['alt'] ?>">
        </div>
    <?php endif; ?>
    <?php if ($team_name): ?>
        <div class="c-team__name">
            <?php echo $team_name ?>
        </div>
    <?php endif; ?>
    <?php if ($team_position): ?>
        <div class="c-team__position">
            <?php echo $team_position ?>
        </div>
    <?php endif; ?>

    <?php if (have_rows('team_social')): ?>
        <div class="c-team__social">
            <?php while (have_rows('team_social')) : the_row();
                $icon = get_sub_field('team_social_icon');
                $link = get_sub_field('team_social_link');
                ?>
                <div class="c-team__social-item">
                    <a href="<?php echo $link['url'] ?>">
                        <img src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt'] ?>">
                    </a>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>

</div>
