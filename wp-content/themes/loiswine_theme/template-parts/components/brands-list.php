<?php
$brands = get_terms([
    'taxonomy' => 'brand',
    'hide_empty' => false,
]);

$category = get_queried_object();
$current_cat_id = $category->term_id;
$current_cat_name = $category->name;

if ($brands): ?>
    <div class="c-brand-sidebar">
        <ul class="c-brand-list">
            <?php foreach ($brands as $brand): ?>
                <li class="c-brand-list__item <?php  echo $current_cat_name == $brand->name ? 'active' : '' ?>">
                    <a href="<?php echo get_term_link($brand->term_id) ?>">
                        <?php echo $brand->name ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif ?>
