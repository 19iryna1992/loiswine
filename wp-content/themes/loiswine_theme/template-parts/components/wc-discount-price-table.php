<?php

$current_currency = get_woocommerce_currency();

$price_row = '';

if($current_currency == 'EUR'):
    $price_row = 'cart_discount_table_euro';
elseif ($current_currency == 'PLN'):
    $price_row = 'cart_discount_table_pln';
endif;


if (have_rows($price_row, 'option')): ?>
    <div class="c-price-table">
        <?php while (have_rows($price_row, 'option')) :
            the_row();

            $price = get_sub_field('price', 'option');
            $discount = get_sub_field('discount', 'option');
           ?>

            <div class="c-price-table__wrap">
                <div class="row m-0 p-0">
                    <div class="col-6 m-0 p-0 ">
                        <div class="c-price-table__item c-price-table__item--border">
                            <?php if ($price): ?>
                                <span><?php echo $price ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-6 m-0 p-0 ">
                        <div class="c-price-table__item c-price-table__item--border">
                            <?php if ($discount): ?>
                                <span><?php echo $discount ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>