<div class="c-wine-detail">
    <?php if (have_rows('single_product_wine_detail')): ?>
        <?php while (have_rows('single_product_wine_detail')) : the_row();
            $label_detail = get_sub_field('label_detail');
            $value_detail = get_sub_field('value_detail'); ?>
            <?php if ($label_detail && $value_detail): ?>
                <div class="c-wine-detail__row">
                    <div class="c-wine-detail_col  c-wine-detail_col_one"><?php echo $label_detail; ?></div>
                    <div class="c-wine-detail_col  c-wine-detail_col_two"><?php echo $value_detail; ?></div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
