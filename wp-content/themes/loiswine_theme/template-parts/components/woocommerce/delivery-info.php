<?php
$delivery_icon = get_field('delivery_icon', 'options');
$delivery_label = get_field('delivery_label', 'options');
$location_icon = get_field('location_icon', 'options');
$location_label = get_field('location_label', 'options');
?>

<ul class="c-delivery">
    <li class="c-delivery__item">
        <?php if ($delivery_icon): ?>
            <img src="<?php echo $delivery_icon['url'] ?>" alt="<?php echo $delivery_icon['alt'] ?>">
        <?php endif; ?>
        <?php if ($delivery_label): ?>
            <span><?php echo $delivery_label ?></span>
        <?php endif; ?>
    </li>
    <li class="c-delivery__item">
        <?php if ($location_icon): ?>
            <img src="<?php echo $location_icon['url'] ?>" alt="<?php echo $location_icon['alt'] ?>">
        <?php endif; ?>
        <?php if ($location_label): ?>
            <span><?php echo $location_label ?></span>
        <?php endif; ?>
    </li>
</ul>