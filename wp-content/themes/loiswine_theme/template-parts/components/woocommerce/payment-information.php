<?php
$images = get_field('payment_information', 'options');
$size = 'size_50_35'; // (thumbnail, medium, large, full or custom size)
if ($images): ?>
    <div class="c-payment-info">
        <p class="c-payment-info__label">
            Payment options:
        </p>
        <ul class="d-flex c-payment-info__list-icons">
            <?php foreach ($images as $image): ?>
                <li class="c-payment-info__item">
                    <img src="<?php echo esc_url($image['sizes']['size_50_35']); ?>"
                         alt="<?php echo esc_attr($image['alt']); ?>"/>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>