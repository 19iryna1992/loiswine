<?php
$rating = get_field('single_product_rating');
$rating_num = $rating + (int)0;

?>
<?php if ($rating_num !== 0): ?>
<div class="c-product-rating">
    <?php if (is_int($rating_num)): ?>
        <ul class="d-flex">
            <?php for ($i = 1; $i <= $rating_num; $i++) { ?>
                <li class="c-product-rating__item">
                    <img src="/wp-content/themes/loiswine_theme/img/icons/star.svg" alt="star">
                </li>
            <?php } ?>
        </ul>
    <?php else: ?>
        <ul class="d-flex">
            <?php
            $rating_num = $rating_num - 0.5;
            for ($i = 1; $i <= $rating_num; $i++) { ?>
                <li class="c-product-rating__item">
                    <img src="/wp-content/themes/loiswine_theme/img/icons/star.svg" alt="star">
                </li>
            <?php } ?>
            <li class="c-product-rating__item">
                <img src="/wp-content/themes/loiswine_theme/img/icons/star_half.svg" alt="star_half">
            </li>
        </ul>
    <?php endif; ?>
</div>
<?php endif; ?>


