<?php
$current_currency = get_woocommerce_currency();

$price_row = '';

if($current_currency == 'EUR'):
    $price_row = 'single_product_price_euro';
elseif ($current_currency == 'PLN'):
    $price_row = 'single_product_price_pln';
endif;


if (have_rows($price_row)): ?>
    <div class="c-price-table">
        <?php while (have_rows($price_row)) :
            the_row();
            $price = get_sub_field('price');
            $discount = get_sub_field('discount');
            $item_price = get_sub_field('item_price');
            $vat = get_sub_field('vat');
            $items_in_box = get_sub_field('items_in_box'); ?>

            <div class="c-price-table__wrap">
                <div class="row m-0 p-0">
                    <div class="col-4 m-0 p-0 ">
                        <div class="c-price-table__item c-price-table__item--border">
                            <?php if ($price): ?>
                                <span><?php echo $price ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-2 m-0 p-0 ">
                        <div class="c-price-table__item c-price-table__item--border">
                            <?php if ($discount): ?>
                                <span><?php echo $discount ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-2 m-0 p-0">
                        <div class="c-price-table__item c-price-table__item--border">
                            <?php if ($item_price): ?>
                                <span><?php echo $item_price ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-2 m-0 p-0 ">
                        <div class="c-price-table__item c-price-table__item--border">
                            <?php if ($vat): ?>
                                <span><?php echo $vat ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-2 m-0 p-0">
                        <div class="c-price-table__item">
                            <?php if ($items_in_box): ?>
                                <span><?php echo $items_in_box ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>