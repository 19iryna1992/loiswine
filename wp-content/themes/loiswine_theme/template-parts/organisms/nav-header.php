<?php
$logo = get_field('header_logo', 'options');
global $woocommerce;
?>
<section class="o-primary-menu d-none d-lg-block">
    <div class="container">
        <div class="row align-items-center">
            <?php if ($logo):
                $logo_size = $logo['sizes']['size_190_70'];
                ?>
                <div class="col-lg-3">
                    <a href="<?php echo get_home_url(); ?>">
                        <img src="<?php echo $logo_size ?>" alt="<?php echo $logo['alt'] ?>">
                    </a>
                </div>
            <?php endif; ?>
            <div class="col-lg-7">
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'menu-primary',
                    'menu_class' => 'o-primary-menu__list',
                ));
                ?>
            </div>
            <div class="col-lg-2">
                <div class="o-primary-menu__cart-wrap">
                    <a href="<?php echo $woocommerce->cart->get_cart_url() ?>" class="o-primary-menu__cart">
                        <img src="/wp-content/themes/loiswine_theme/img/icons/shopping-bag-black.svg" alt="cart">|
                        <span class=""><?php echo sprintf($woocommerce->cart->get_cart_subtotal()); ?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
