<section class="o-main-slider">
    <?php if (have_rows('main_slider')): ?>
        <div class="swiper-slider JS--main-slider">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php while (have_rows('main_slider')) : the_row();
                        $background_img = get_sub_field('main_background');
                        $image = get_sub_field('image');
                        $image_label = get_sub_field('image_label');
                        $label_color = get_sub_field('label_color');
                        $title = get_sub_field('title');
                        $text_color = get_sub_field('title_color');
                        $link = get_sub_field('link');
                        $wysiwyg = get_sub_field('description');
                        ?>
                        <div class="swiper-slide o-home-banner"  style="background-image: url('<?php echo $background_img ? $background_img['url'] : '' ?>')">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-md-5 order-md-2">
                                        <div class="o-home-banner__img">
                                            <?php if ($image):
                                                $img_size = $image['sizes']['size_355_520'];
                                                ?>
                                                <img src="<?php echo $img_size ?>" alt="<?php echo $image['alt'] ?>">
                                            <?php endif; ?>
                                            <?php if($image_label): ?>
                                                <span class="o-home-banner__img-label" style="color: <?php echo $label_color ?>"><?php echo $image_label ?></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-10 col-md-6 order-md-1">
                                        <div class="o-home-banner__intro" style="color: <?php echo $text_color ?>">
                                            <?php if ($title): ?>
                                                <h1 class="o-home-banner__title"><?php echo $title ?></h1>
                                            <?php endif; ?>
                                            <?php if ($wysiwyg): ?>
                                                <div class="o-home-banner__wysiwyg">
                                                    <?php echo $wysiwyg ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($link): ?>
                                                <a class="o-home-banner__link"
                                                   href="<?php echo $link['url'] ?>"> <?php echo $link['title'] ?></a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>

    <?php endif; ?>
</section>

