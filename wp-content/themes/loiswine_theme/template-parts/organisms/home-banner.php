<?php
$background_img = get_field('home_banner_background');
$image = get_field('home_banner_image');
$image_label = get_field('home_banner_image_label');
$title = get_field('home_banner_title');
$text_color = get_field('home_banner_title_color');
$link = get_field('home_banner_link');
$wysiwyg = get_field('home_banner_text');

?>


<section class="o-home-banner"
         style="background-image: url('<?php echo $background_img ? $background_img['url'] : '' ?>')">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 order-md-2">
                <div class="o-home-banner__img">
                    <?php if ($image):
                        $img_size = $image['sizes']['size_355_520'];
                        ?>
                        <img src="<?php echo $img_size ?>" alt="<?php echo $image['alt'] ?>">
                    <?php endif; ?>
                    <?php if($image_label): ?>
                    <span class="o-home-banner__img-label" style="color: <?php echo $text_color ?>"><?php echo $image_label ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12 col-md-6 order-md-1">
                <div class="o-home-banner__intro" style="color: <?php echo $text_color ?>">
                    <?php if ($title): ?>
                        <h1 class="o-home-banner__title"><?php echo $title ?></h1>
                    <?php endif; ?>
                    <?php if ($wysiwyg): ?>
                        <div class="o-home-banner__wysiwyg">
                            <?php echo $wysiwyg ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($link): ?>
                        <a class="o-home-banner__link"
                           href="<?php echo $link['url'] ?>"> <?php echo $link['title'] ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
