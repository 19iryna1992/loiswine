<?php
$logo = get_field('header_logo', 'options');
global $woocommerce;
?>

<section class="o-mobile-menu d-lg-none">
    <div class="container">
        <div id="menu_open" class="o-mobile-menu__container">
            <div class="o-mobile-menu__top">
                <div class="o-header-top__wpml">
                    <?php echo do_shortcode('[wpml_language_selector_widget]') ?>
                </div>
                <div class="o-header-top__login">
                    <?php if (is_user_logged_in()) { ?>
                        <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                           title="<?php _e('My Account', 'loiswine'); ?>">
                                <span class="o-header-top__login-icon">
                                    <img src="/wp-content/themes/loiswine_theme/img/icons/login-white.svg" />
                                </span>
                            <?php _e('My Account', 'loiswine'); ?></a>
                    <?php } else { ?>
                        <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                           title="<?php _e('Login', 'loiswine'); ?>">
                                <span class="o-header-top__login-icon">
                                    <img src="/wp-content/themes/loiswine_theme/img/icons/login-white.svg" />
                                </span>
                            <?php _e('Login', 'loiswine'); ?></a>
                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="o-mobile-menu__toggle-close__wrap">
                        <button id="toggle_menu_close" class="o-mobile-menu__toggle">
                            <img src="/wp-content/themes/loiswine_theme/img/icons/black-close.svg" alt="menu">
                        </button>
                    </div>
                </div>
                <div class="col-12">
                    <?php wp_nav_menu(array(
                        'theme_location' => 'menu-primary',
                        'menu_class' => 'o-mobile-menu__list',
                    ));
                    ?>
                </div>
            </div>

        </div>
        <div class="row">
            <?php if ($logo):
                $logo_size = $logo['sizes']['size_190_70'];
                ?>
                <div class="col-6">
                    <div class="o-mobile-menu__logo">
                        <a href="<?php echo get_home_url(); ?>">
                            <img src="<?php echo $logo_size ?>" alt="<?php echo $logo['alt'] ?>">
                        </a>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-6 d-flex align-items-center justify-content-end">
                <div class="o-mobile-menu__toggle-wrap">
                    <button id="toggle_menu" class="o-mobile-menu__toggle">
                        <img src="/wp-content/themes/loiswine_theme/img/icons/menu-black.svg" alt="menu">
                    </button>
                </div>
                <a href="<?php echo $woocommerce->cart->get_cart_url() ?>" class="o-mobile-menu__cart">
                    <img src="/wp-content/themes/loiswine_theme/img/icons/shopping-bag-black.svg" alt="cart">
                    <span class=""><?php echo sprintf($woocommerce->cart->get_cart_subtotal()); ?></span>
                </a>
            </div>
        </div>
    </div>
</section>
