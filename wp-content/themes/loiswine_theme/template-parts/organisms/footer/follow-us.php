<?php
$title = get_field('footer_social_title', 'options');
?>

<?php if (have_rows('footer_social_links', 'options')): ?>
    <section class="s-follow-us">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php while (have_rows('footer_social_links', 'options')) : the_row();
                        $icon = get_sub_field('social_links_icon', 'options');
                        $url = get_sub_field('social_links_url', 'options');
                        ?>

                        <a href="<?php echo $url ?>">
                            <div class="s-follow-us__icon">
                                <img src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt']; ?>">
                            </div>
                        </a>
                    <?php endwhile; ?>
                    <?php if( $title): ?>
                        <span  class="s-follow-us__title"><?php echo $title ?></span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

