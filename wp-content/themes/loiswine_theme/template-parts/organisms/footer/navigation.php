<?php
$title_contact = get_field('footer_contacts_title', 'option');
$title_info_nav = get_field('footer_information_menu_title', 'option');
$title_shop_nav = get_field('footer_shop_menu_title', 'option');
$title_account_nav = get_field('account_contacts_title', 'option');
?>
<section class="s-footer-nav">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ($title_contact): ?>
                    <h3 class="s-footer-nav__title"><?php echo $title_contact ?></h3>
                <?php endif ?>
                <?php if (have_rows('footer_location', 'option')): ?>
                    <div class="s-footer-nav__location">
                        <?php while (have_rows('footer_location', 'option')): the_row();

                            $icon = get_sub_field('footer_address_icon');
                            $title = get_sub_field('footer_location_title');
                            $text = get_sub_field('footer_address');
                            ?>

                            <div class="s-footer-nav__wrap">
                                <?php if ($title): ?>
                                    <span class="s-footer-nav__location-title">
                                        <?php echo $title ?>
                                    </span>
                                <?php endif ?>
                                <?php if ($icon): ?>
                                    <img src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt'] ?>">
                                <?php endif; ?>
                            </div>
                            <?php if ($text): ?>
                                <div class="s-footer-nav__address">
                                    <?php echo $text ?>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
                <?php if (have_rows('footer_mail_group', 'option')): ?>
                    <div class="s-footer-nav__mail">
                        <?php while (have_rows('footer_mail_group', 'options')): the_row();

                            $mail = get_sub_field('mail');
                            $label = get_sub_field('label');
                            ?>
                            <?php if ($mail): ?>
                                <div class="s-footer-nav__wrap">
                                    <a href="mailto:<?php echo $mail ?>"><?php echo $mail ?></a>
                                </div>
                            <?php endif; ?>
                            <?php if ($label): ?>
                                <div class="s-footer-nav__wrap">
                                <span class="s-footer-nav__mail-label">
                                    <?php echo $label ?>
                                </span>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>

                <!-- Location two -->

                <?php if (have_rows('footer_location_two', 'option')): ?>
                    <div class="s-footer-nav__location">
                        <?php while (have_rows('footer_location_two', 'option')): the_row();

                            $icon = get_sub_field('footer_address_icon');
                            $title = get_sub_field('footer_location_title');
                            $text = get_sub_field('footer_address');
                            ?>

                            <div class="s-footer-nav__wrap">
                                <?php if ($title): ?>
                                    <span class="s-footer-nav__location-title">
                                    <?php echo $title ?>
                                </span>
                                <?php endif; ?>
                                <?php if ($icon): ?>
                                    <img src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt'] ?>">
                                <?php endif; ?>
                            </div>
                            <?php if ($text): ?>
                                <div class="s-footer-nav__address">
                                    <?php echo $text ?>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>

                <?php if (have_rows('footer_mail_group_two', 'option')): ?>
                    <div class="s-footer-nav__mail">
                        <?php while (have_rows('footer_mail_group_two', 'option')): the_row();

                            $mail = get_sub_field('mail');
                            $label = get_sub_field('label');
                            ?>
                            <?php if ($mail): ?>
                                <div class="s-footer-nav__wrap">
                                    <a href="mailto:<?php echo $mail ?>"><?php echo $mail ?></a>
                                </div>
                            <?php endif; ?>
                            <?php if ($label): ?>
                                <div class="s-footer-nav__wrap">
                                <span class="s-footer-nav__mail-label">
                                    <?php echo $label ?>
                                </span>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>

            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ($title_info_nav): ?>
                    <h3 class="s-footer-nav__title"><?php echo $title_info_nav ?></h3>
                <?php endif; ?>
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer-menu-info',
                    'menu_class' => 's-footer-nav__list',
                ));
                ?>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ($title_shop_nav): ?>
                    <h3 class="s-footer-nav__title"><?php echo $title_shop_nav ?></h3>
                <?php endif; ?>
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer-menu-shop',
                    'menu_class' => 's-footer-nav__list',
                ));
                ?>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ($title_account_nav): ?>
                    <h3 class="s-footer-nav__title"><?php echo $title_account_nav ?></h3>
                <?php endif; ?>
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer-menu-account',
                    'menu_class' => 's-footer-nav__list',
                ));
                ?>
            </div>
        </div>
    </div>
</section>
