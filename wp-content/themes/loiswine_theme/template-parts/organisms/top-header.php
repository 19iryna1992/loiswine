<?php
$mail = get_field('top_header_mail', 'options');

?>

<section class="o-header-top d-none d-lg-block">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <?php if ($mail): ?>
                    <a class="o-header-top__mail" href="mailto:<?php echo $mail ?>"><?php echo $mail ?></a>
                <?php endif; ?>
            </div>
            <div class="col-lg-6 d-flex justify-content-end">
                <div class="o-header-top__wraper">
                    <div class="o-header-top__wpml">
                        <?php echo do_shortcode('[wpml_language_selector_widget]') ?>
                    </div>
                    <div class="o-header-top__login">
                        <?php if (is_user_logged_in()) { ?>
                            <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                               title="<?php _e('My Account', 'loiswine'); ?>">
                                <span class="o-header-top__login-icon">
                                     <img src="/wp-content/themes/loiswine_theme/img/icons/login-white.svg" />
                                </span>
                                <?php _e('My Account', 'loiswine'); ?></a>
                        <?php } else { ?>
                            <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                               title="<?php _e('Login', 'loiswine'); ?>">
                                <img src="/wp-content/themes/loiswine_theme/img/icons/login-white.svg"/>
                                <?php _e('Login', 'loiswine'); ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

