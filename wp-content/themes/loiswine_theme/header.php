<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package loiswine
 */
$current_user = wp_get_current_user();
?>
    <!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="profile" href="https://gmpg.org/xfn/11">

        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <?php get_template_part('template-parts/sections/home/modal-window'); ?>
    <header class="o-header">
        <?php get_template_part('template-parts/organisms/mobile-menu'); ?>

        <?php get_template_part('template-parts/organisms/top-header'); ?>

        <?php get_template_part('template-parts/organisms/nav-header'); ?>
    </header>

    <div id="content" class="site-content">
