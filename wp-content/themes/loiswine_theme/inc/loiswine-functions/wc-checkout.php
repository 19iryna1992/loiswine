<?php

add_filter('woocommerce_cart_needs_payment', 'disabled_payment');
function disabled_payment()
{
    return false;
}

add_filter('woocommerce_checkout_fields', 'loiswine_checkout_required_fields', 9999);

function loiswine_checkout_required_fields($f)
{

    $f['billing']['billing_company']['required'] = true;

    return $f;
}