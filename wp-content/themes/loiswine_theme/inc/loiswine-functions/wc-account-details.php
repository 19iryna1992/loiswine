<?php
add_filter('woocommerce_save_account_details_required_fields', 'loiswine_myaccount_required_fields');

function loiswine_myaccount_required_fields( $account_fields ) {

    unset( $account_fields['account_display_name'] );

    return $account_fields;

}

/*
add_action('woocommerce_edit_account_form_start', 'loiswine_add_field_edit_account_form');
function loiswine_add_field_edit_account_form()
{


    woocommerce_form_field(
        'company_tin',
        array(
            'type'        => 'text',
            'required'    => true, // just adds an "*"
            'label'       => 'Company TIN'
        ),
        get_user_meta(get_current_user_id(), 'company_tin', true) // get the data
    );

}*/

add_action('woocommerce_save_account_details', 'loiswine_save_account_details');
function loiswine_save_account_details($user_id)
{
   // update_user_meta($user_id, 'company_tin', sanitize_text_field($_POST['company_tin']));
    update_user_meta($user_id, 'country', sanitize_text_field($_POST['country']));
}

add_filter('woocommerce_save_account_details_required_fields', 'loiswine_make_field_required');
function loiswine_make_field_required($required_fields)
{
  //  return $array, account_last_name => __, account_display_name => __, account_email => __);

}


//  my account menu (remove dashboard)


add_filter( 'woocommerce_account_menu_items', 'misha_remove_my_account_dashboard' );
function misha_remove_my_account_dashboard( $menu_links ){

    unset( $menu_links['dashboard'] );
    return $menu_links;

}

add_action('template_redirect', 'misha_redirect_to_orders_from_dashboard' );

function misha_redirect_to_orders_from_dashboard(){

    if( is_account_page() && empty( WC()->query->get_current_endpoint() ) ){
        wp_safe_redirect( wc_get_account_endpoint_url( 'orders' ) );
        exit;
    }

}