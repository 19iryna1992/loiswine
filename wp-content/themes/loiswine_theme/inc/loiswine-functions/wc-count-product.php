<?php // Adding and displaying additional product quantity custom fields
add_action( 'woocommerce_product_options_pricing', 'additional_product_pricing_option_fields', 50 );
function additional_product_pricing_option_fields() {
$domain = "woocommerce";
global $post;
echo '</div><div class="options_group pricing">';
    woocommerce_wp_text_input( array(
    'id'            => '_input_qty',
    'label'         => __("Quantity per pack", $domain ),
    'placeholder'   => '',
    'description'   => __("Quantity of product units in the package", $domain ),
    'desc_tip'      => true,
    ) );
    woocommerce_wp_text_input( array(
    'id'            => '_step_qty',
    'label'         => __("Step", $domain ),
    'placeholder'   => '',
    'description'   => __("Step product units when ordering", $domain ),
    'desc_tip'      => true,
    ) );
    }
    // Saving product custom quantity values
    add_action( 'woocommerce_admin_process_product_object', 'save_product_custom_meta_data', 100, 1 );
    function save_product_custom_meta_data( $product ){
    if ( isset( $_POST['_input_qty'] ) )
    $product->update_meta_data( '_input_qty', sanitize_text_field($_POST['_input_qty']) );
    if ( isset( $_POST['_step_qty'] ) )
    $product->update_meta_data( '_step_qty', sanitize_text_field($_POST['_step_qty']) );
    }
    // Set product quantity field by product
    add_filter( 'woocommerce_quantity_input_args', 'custom_quantity_input_args', 10, 2 );
    function custom_quantity_input_args( $args, $product ) {
    if( $product->get_meta('_input_qty') ){
    $args['input_value'] = is_cart() ? $args['input_value'] : $product->get_meta('_input_qty');
    $args['min_value']   = $product->get_meta('_input_qty');
    }
    if( $product->get_meta('_step_qty') ){
    $args['step'] = $product->get_meta('_step_qty');
    }
    return $args;
    }