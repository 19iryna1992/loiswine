<?php

add_action('after_setup_theme', 'woocommerce_support');

function woocommerce_support()
{
    add_theme_support('woocommerce');

    // add_theme_support('wc-product-gallery-slider');
}

function change_view_cart( $params, $handle )
{
    switch ($handle) {
        case 'wc-add-to-cart':
            $params['i18n_view_cart'] = ""; //chnage Name of view cart button
            $params['cart_url'] = ""; //change URL of view cart button
            break;
    }
    return $params;
}
add_filter( 'woocommerce_get_script_data', 'change_view_cart',10,2 );

remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);

remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);



add_filter( 'woocommerce_loop_add_to_cart_link', 'replacing_add_to_cart_button', 10, 2 );
function replacing_add_to_cart_button( $button, $product  ) {
    $button_text = __("add to cart", "woocommerce");
    $button = '<a class="button" href="?add-to-cart=' . $product->get_id() . '">' . $button_text . '<span class="icon"></span>' . '</a>';

    return $button;
}

add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);
function sb_woo_remove_reviews_tab($tabs) {

    unset($tabs['reviews']);

    return $tabs;
}


add_filter( 'woocommerce_after_shop_loop_item_title', 'wpspec_show_product_description', 5 );

function wpspec_show_product_description() {
    echo '<div class="woo-product-short-desc">' . get_the_excerpt() . '</div>';
}

// Remove cross-sells at cart

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

//add_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
