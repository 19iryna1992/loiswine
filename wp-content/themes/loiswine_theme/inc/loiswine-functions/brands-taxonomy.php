<?php
function add_brand_taxonomies() {
    register_taxonomy('brand',
        array('product'),
        array(
            'hierarchical' => true,
            'labels' => array(
                'name' => 'Product Brand',
                'singular_name' => 'Brand',
                'search_items' =>  'Find brand',
                'all_items' => 'All brands',
                'parent_item' => null,
                'parent_item_colon' => null,
                'edit_item' => 'Edit brand',
                'update_item' => 'Update brand',
                'add_new_item' => 'Add new brand',
                'new_item_name' => 'Name brand',
                'add_or_remove_items' => 'Add or remove brand',
                'menu_name' => 'Brands'
            ),
            'public' => true,
            'show_in_nav_menus' => true,
            'show_ui' => true,
            'show_tagcloud' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'brands',
                'hierarchical' => false

            ),
        )
    );
}
add_action( 'init', 'add_brand_taxonomies', 0 );
