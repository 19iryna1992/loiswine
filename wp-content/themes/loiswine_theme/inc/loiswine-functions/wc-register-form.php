<?php

global $user;

add_action('woocommerce_register_form_start', 'loiswine_add_before_register_form_field');

function loiswine_add_before_register_form_field()
{

    woocommerce_form_field(
        'billing_company',
        array(
            'type' => 'text',
            'required' => true, // just adds an "*"
            'label' => __('Company name'),
        ),
        (isset($_POST['billing_company']) ? $_POST['billing_company'] : '')
    );

    woocommerce_form_field(
        'billing_company_tin',
        array(
            'type' => 'text',
            'required' => true, // just adds an "*"
            'label' => __('VAT EU'),
        ),
        (isset($_POST['billing_company_tin']) ? $_POST['billing_company_tin'] : '')
    );

    $countries_obj = new WC_Countries();
    $countries = $countries_obj->__get('countries');

    wp_enqueue_script('wc-country-select');

    woocommerce_form_field('billing_country', array(
        'type' => 'country',
        'class' => array('chzn-drop'),
        'label' => __('Country'),
        'required' => true,
        'clear' => false,
    ));

}


add_action('woocommerce_register_form', 'loiswine_add_after_register_form_field');

function loiswine_add_after_register_form_field()
{

    woocommerce_form_field(
        'customer_regulations',
        array(
            'type' => 'checkbox',
            'required' => true, // just adds an "*"
            'label' => __('I accept the online store regulations')
        ),
        (isset($_POST['customer_regulations']) ? $_POST['customer_regulations'] : '')
    );

}

add_action('woocommerce_register_post', 'lostwine_validate_fields', 10, 3);

function lostwine_validate_fields($username, $email, $errors)
{

    if (empty($_POST['billing_company'])) {
        $errors->add('company_name_error', 'Error!');
    }
    if (empty($_POST['billing_company_tin'])) {
        $errors->add('billing_company_tin', 'Error!');
    }
    if (empty($_POST['billing_country'])) {
        $errors->add('billing_country', ' Error!');
    }
    if (empty($_POST['customer_regulations'])) {
        $errors->add('customer_regulations_error', ' Error!');
    }

}


add_action('woocommerce_created_customer', 'loiswine_save_register_fields');

function loiswine_save_register_fields($customer_id)
{

    if (isset($_POST['billing_company'])) {
        update_user_meta($customer_id, 'billing_company', wc_clean($_POST['billing_company']));
    }

    if (isset($_POST['billing_company_tin'])) {
        update_user_meta($customer_id, 'billing_company_tin', wc_clean($_POST['billing_company_tin']));
    }

    if (isset($_POST['customer_regulations'])) {
        update_user_meta($customer_id, 'customer_regulations', wc_clean($_POST['customer_regulations']));
    }


    if (isset($_POST['billing_country'])) {
        update_user_meta($customer_id, 'billing_country', wc_clean($_POST['billing_country']));
    }
}

add_filter('woocommerce_customer_meta_fields', 'loiswine_admin_address_field');

function loiswine_admin_address_field($admin_fields)
{

    $admin_fields['billing']['fields']['billing_company_tin'] = array(
        'label' => 'VAT EU',
    );
    return $admin_fields;

}