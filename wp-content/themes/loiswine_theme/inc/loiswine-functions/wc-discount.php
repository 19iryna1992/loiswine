<?php
add_action('woocommerce_cart_calculate_fees', 'progressive_discount_based_on_cart_total', 10, 1);
function progressive_discount_based_on_cart_total($cart_object)
{

    // ACF
    $discount_10 = get_field('discount_10', 'options');
    $discount_15 = get_field('discount_15', 'options');
    $discount_20 = get_field('discount_20', 'options');

    $current_currency = get_woocommerce_currency();

    $cart_total = $cart_object->subtotal; // Cart subtotal
    $percent = 0;

    if ($current_currency == 'EUR'):
        if ($cart_total > $discount_20['discount_euro']):
            $percent = 20;
        elseif ($cart_total >= $discount_15['discount_euro'] && $cart_total < $discount_20['discount_euro']):
            $percent = 15;
        elseif ($cart_total >= $discount_10['discount_euro'] && $cart_total < $discount_15['discount_euro']):
            $percent = 10;
        else:
            $percent = 0;
        endif;
    elseif ($current_currency == 'PLN'):
        if ($cart_total > $discount_20['discount_pln']):
            $percent = 20;
        elseif ($cart_total >= $discount_15['discount_pln'] && $cart_total < $discount_20['discount_pln']):
            $percent = 15;
        elseif ($cart_total >= $discount_10['discount_pln'] && $cart_total < $discount_15['discount_pln']):
            $percent = 10;
        else:
            $percent = 0;
        endif;
    endif;

    if ($percent != 0) {
        $discount = ($cart_total * $percent) / 100;
        $cart_object->add_fee(__('Discount') . "($percent%)", -$discount, false, 'zero-rate');
    }
}

// exclude discount from taxes
function excludeCartFeesTaxes($taxes, $fee, $cart)
{
    return [];
}

add_action('woocommerce_cart_totals_get_fees_from_cart_taxes', 'excludeCartFeesTaxes', 10 ,3);