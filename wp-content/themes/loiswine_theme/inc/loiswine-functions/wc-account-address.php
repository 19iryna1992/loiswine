<?php
add_filter('woocommerce_default_address_fields', 'loiswine_add_build_address_field');

function loiswine_add_build_address_field($fields)
{

    $fields['company_tin'] = array(

        'label' => 'VAT EU',
        'required' => true,
        'priority' => 30,
    );

    return $fields;
}

add_filter('woocommerce_default_address_fields', 'loiswine_add_shiping_address_field');

function loiswine_add_shiping_address_field($fields)
{

    $fields['shipping_phone'] = array(
        'label' => 'Phone',
        'required' => true,
        'priority' => 75,
    );
    return $fields;
}


/**
 * Process the checkout
 */
add_action('woocommerce_checkout_process', 'loiswine_checkout_field_process');

function loiswine_checkout_field_process()
{
    // Check if set, if its not set add an error.
    if (!$_POST['billing_company_tin'])
        wc_add_notice(__('Company TIN is compulsory. Please enter a value'), 'error');
}


/**
 * Update the order meta with field value
 */
add_action('woocommerce_checkout_update_order_meta', 'loiswine_checkout_field_update_order_meta');

function loiswine_checkout_field_update_order_meta($order_id)
{
    if (!empty($_POST['billing_company_tin'])) {
        update_post_meta($order_id, 'billing_company_tin', $_POST['billing_company_tin']);
    }
}

function loiswine_woocommerce_checkout_update_user_meta( $customer_id, $posted ) {
    if (isset($posted['billing_company_tin'])) {
        $dob = sanitize_text_field( $posted['billing_company_tin'] );
        update_user_meta( $customer_id, 'billing_company_tin', $dob);
    }
}
add_action( 'woocommerce_checkout_update_user_meta', 'loiswine_woocommerce_checkout_update_user_meta', 10, 2 );



/**
 * Display field value on the order edit page
 */

/*add_action('woocommerce_admin_order_data_after_billing_address', 'my_custom_billing_fields_display_admin_order_meta', 10, 1);

function my_custom_billing_fields_display_admin_order_meta($order) {
    echo '<p><strong>' . __('Billing Gls Name') . ':</strong><br> ' . get_post_meta($order->id, '_billing_gls_name', true) . '</p>';
}*/

add_action('woocommerce_admin_order_data_after_billing_address', 'loiswine_checkout_field_display_admin_order_meta', 10, 1);

function loiswine_checkout_field_display_admin_order_meta($order)
{

    echo '<p><strong>' . __('Company TIN') . ':</strong> <br/>' . get_post_meta($order->id, 'billing_company_tin', true) . '</p>';
    //echo '<p><b>Shipping Phone:</b> ' . get_post_meta($order->get_id(), 'shipping_phone', true) . '</p>';
}


function modify_woocommerce_default_address_fields($fields)
{
    $fields['company']['required'] = true;

    return $fields;
}

add_filter('woocommerce_default_address_fields', 'modify_woocommerce_default_address_fields', 100, 1);


/*-------------------------
Shipping address form
-------------------------*/

add_filter('woocommerce_shipping_fields', 'loiswine_remove_shipping_fields');

function loiswine_remove_shipping_fields($fields)
{

    unset($fields['shipping_first_name']);
    unset($fields['shipping_last_name']);
    unset($fields['shipping_company']);
    unset($fields['shipping_company_tin']);
    return $fields;

}

/*-------------------------
Billing address form
-------------------------*/

add_filter('woocommerce_billing_fields', 'loiswine_remove_billing_fields');

function loiswine_remove_billing_fields($fields)
{

    unset($fields['billing_shipping_phone']);
    return $fields;

}
