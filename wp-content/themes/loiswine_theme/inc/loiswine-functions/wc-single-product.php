<?php
global $product;

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);

add_action('woocommerce_single_product_summary', 'woocommerce_product_loop_tags', 31);
function woocommerce_product_loop_tags()
{
    global $post, $product;
    $product->get_title();
    echo $product->get_tags(' | ', '<span class="product-tags">' . __($tag_count, 'woocommerce') . ' ', '.</span>');
    echo '<span>' . get_template_part('template-parts/components/woocommerce/product-rating') . '</span>';
}