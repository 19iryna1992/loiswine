<?php
/**
* @snippet       Sort Products By Stock Status - WooCommerce Shop
* @how-to        Get CustomizeWoo.com FREE
* @author        Rodolfo Melogli
* @compatible    WooCommerce 3.9
* @donate $9     https://businessbloomer.com/bloomer-armada/
*/

add_filter( 'woocommerce_get_catalog_ordering_args', 'bbloomer_first_sort_by_stock_amount', 9999 );

function bbloomer_first_sort_by_stock_amount( $args ) {
$args['orderby'] = 'meta_value';
$args['order'] = 'ASC';
$args['meta_key'] = '_stock_status';
return $args;
}