<?php /* Template Name: Winemakers Page Template */

get_header();

// ACF vars

$image = get_field('winemakers_image');
$description = get_field('winemakers_description');
?>
    <main id="main" role="main" tabindex="-1">
        <div class="JS-brand-sidebar-mob c-brand-sidebar-mob d-lg-none">
            <div class="c-brand-sidebar-mob__btn-wrap">
                <button class="c-brand-sidebar-mob__btn"><img
                            src="/wp-content/themes/loiswine_theme/img/icons/black-filter.svg"
                            alt="loiswine_filter">
                </button>
            </div>
            <?php get_template_part('template-parts/components/brands-list') ?>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="c-breadcrumb">
                        <?php woocommerce_breadcrumb(); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="d-none d-lg-block col-lg-3">
                    <?php get_template_part('template-parts/components/brands-list') ?>
                </div>
                <div class="col-12 col-lg-9">
                    <div class="c-brand__content">
                        <div class="row">
                            <div class="col-12">
                                <div class="c-brand__title">
                                    <h2 class="s-products-cat__title"><?php _e('Winemakers', 'loiswine'); ?></h2>
                                </div>
                            </div>
                            <?php if ($image): ?>
                                <div class="col-12">
                                    <div class="c-brand__img">
                                        <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="col-12">
                                <div class="c-brand__description">
                                    <?php echo $description ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>

<?php get_footer(); ?>