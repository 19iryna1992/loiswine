<?php /* Template Name: Contacts Page Template */

get_header(); ?>

    <main id="main" role="main" tabindex="-1">

        <?php get_template_part('template-parts/components/breadcrumbs'); ?>

        <?php get_template_part('template-parts/sections/teams'); ?>

        <?php get_template_part('template-parts/sections/contacts'); ?>

    </main>

<?php get_footer(); ?>