<?php /* Template Name: FAQ Page Template */

get_header(); ?>

    <main id="main" role="main" tabindex="-1">
        <?php get_template_part('template-parts/components/breadcrumbs'); ?>

        <?php get_template_part('template-parts/sections/faq'); ?>

    </main>


<?php get_footer(); ?>