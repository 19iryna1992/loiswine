<?php /* Template Name: About Page Template */

get_header(); ?>

    <main id="main" role="main" tabindex="-1">

        <?php get_template_part('template-parts/components/breadcrumbs'); ?>

        <?php get_template_part('template-parts/sections/about'); ?>

    </main>



<?php get_footer(); ?>