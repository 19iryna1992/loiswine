
<?php /* Template Name: Home Page Template */
get_header(); ?>


<main id="main" role="main" tabindex="-1">
    <?php get_template_part('template-parts/organisms/main-slider'); ?>
    <?php get_template_part('template-parts/sections/home/info-block'); ?>
    <?php get_template_part('template-parts/sections/home/wc-product-category-one'); ?>
    <?php get_template_part('template-parts/sections/home/wine-story'); ?>
    <?php get_template_part('template-parts/sections/home/wine-testing'); ?>
    <?php get_template_part('template-parts/sections/home/events'); ?>
    <?php get_template_part('template-parts/sections/home/instagram'); ?>
</main>

<?php get_footer(); ?>

