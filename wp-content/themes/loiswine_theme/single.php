<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package loiswine
 */

get_header();

?>


<main id="main" role="main" tabindex="-1">

    <?php get_template_part('template-parts/components/breadcrumbs'); ?>

    <?php if (have_posts()): ?>
        <section class="s-single-post">
            <div class="container">
                <div class="row justify-content-center">
                    <?php while (have_posts()): the_post(); ?>
                        <div class="col-12 col-lg-10">
                            <div class="s-single-post__img" >
                                <?php echo get_the_post_thumbnail() ?>
                            </div>
                            <div class="s-single-post__title">
                                <h3>
                                    <?php echo get_the_title(); ?>
                                </h3>
                                <span class="c-post__data">
                                            <?php echo get_the_date(); ?>
                                        </span>
                            </div>
                            <div class="s-single-post__content">
                                <?php echo get_the_content() ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata() ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
</main>


<?php get_footer(); ?>
